require "spec_helper"
require 'rails_helper'


RSpec.describe User, type: :model do	
	let(:comment_attribute){
	    ({content: "My Comment", user_id: "1" , post_id: "1"})
	}

	it "has a name" do
		@user = User.create
		@user.should respond_to(:name)
	end

	it "has many posts" do
		@p1 = Post.create(content: "First post")
		@p2 = Post.create(content: "sceond post")
		@user = User.create(name: "First user")
		@p1.user = @user
		@p1 .save
		@p2.user = @user
		@p2 .save

		@user.posts.should eq([@p1,@p2])
	end
	
	it "has many comments" do
		@c1  = Comment.new comment_attribute 
		@c2  = Comment.new comment_attribute 
		@user = User.create(name: "First user")
		@c1.user = @user
		@c1 .save
		@c2.user = @user
		@c2 .save

		@user.comments.should eq([@c1,@c2])
	end
end