require "spec_helper"
require 'rails_helper'

describe "Comment" do
	it "is has a content" do
		@comment = Comment.create
		@comment.should respond_to :content
	end

	it "belongs to user" do
		@comment = Comment.create(content: "my_comment")
		@user = User.create(name: "my_user")
		@user.comments << @comment

		@comment.user.should eq(@user)
	end

	it "belongs to post" do
		@comment = Comment.create(content: "my_comment")
		@post = Post.create(content: "my_post")
		@post.comments << @comment

		@comment.post.should eq(@post)
	end

	it "falied if not belong to post" do
		expect{Comment.create!(content: "dummy" , post_id: "1") } .to raise_error(ActiveRecord::RecordInvalid)
	end

	it "falied if not belong to user" do
		expect{Comment.create!(content: "dummy" , user_id: "1") }.to raise_error(ActiveRecord::RecordInvalid)
	end

end