require "spec_helper"
require 'rails_helper'

RSpec.describe Post, type: :model do	

	let(:comment_attribute){
	    ({content: "My Comment", user_id: "1" , post_id: "1"})
	}

	it "is has a content" do
		@post  = Post.create
		@post.should respond_to :content
	end

	it "belongs to user" do
		@user  = User.create(name: "my_user")
		@post = Post.create(content: "First post")
		@user.posts << @post
		
		@post.user.should eq(@user)
	end

	it "has many comments" do
		@c1  = Comment.new comment_attribute 
		@c2  = Comment.new comment_attribute 
		@post = Post.create(content: "my_post")

		@c1.post = @post
		expect {@c1.save!}.to change{@post.comments.count}.by(1)

		@c2.post = @post
		expect {@c2.save!}.to change{@post.comments.count}.from(1).to(2)		
	end
end