require 'rails_helper'

RSpec.describe "comments/index", type: :view do
  let(:comment_attribute){
      ({content: "My Comment", user_id: "1" , post_id: "1"})
  }

  before(:each) do
    assign(:comments, [
      Comment.create!(
        comment_attribute
      ),
      Comment.create!(
        comment_attribute
      )
    ])
  end

  it "renders a list of comments" do
    render
    assert_select "tr>td", :text => "My Comment".to_s, :count => 2
  end
end
