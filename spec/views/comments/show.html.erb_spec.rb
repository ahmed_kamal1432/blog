require 'rails_helper'

RSpec.describe "comments/show", type: :view do
	let(:comment_attribute){
	    ({content: "My Comment", user_id: "1" , post_id: "1"})
	}

  before(:each) do
    @comment = assign(:comment, Comment.create!(
	comment_attribute		
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Content/)
  end
end
