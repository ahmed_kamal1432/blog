require 'rails_helper'

RSpec.describe "comments/edit", type: :view do
  let(:comment_attribute){
      ({content: "My Comment", user_id: "1" , post_id: "1"})
  }

  before(:each) do
    @comment = assign(:comment, Comment.create!(
      comment_attribute
    ))
  end

  it "renders the edit comment form" do
    render

    assert_select "form[action=?][method=?]", comment_path(@comment), "post" do

      assert_select "input#comment_content[name=?]", "comment[content]"
    end
  end
end
